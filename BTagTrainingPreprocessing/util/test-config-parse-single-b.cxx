#include "src/SingleBTagConfig.hh"
#include "src/ConfigFileTools.hh"

#include <nlohmann/json.hpp>

#include <filesystem>
#include <iostream>

const std::string CFG_GROUP = "dumper";

bool has_cfg_group(const nlohmann::ordered_json& j) {
  if (j.count(CFG_GROUP)) return true; 
  return false;
}

bool is_combined(const nlohmann::ordered_json& j) {
  for (const auto& entry: j) {
    if (!has_cfg_group(entry)) return false;
  }
  return true;
}

int main(int narg, char* argv[]) {
  namespace fs = std::filesystem;
  if (narg != 2) {
    std::cout << "usage: " << argv[0] << " <json_file>" << std::endl;
    return 1;
  }

  fs::path cfg_path(argv[1]);
  if (!fs::exists(cfg_path)) return 2;
  std::ifstream cfg_stream(cfg_path);
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  ConfigFileTools::combine_files(nlocfg, cfg_path.parent_path());
  if (is_combined(nlocfg)) {
    for (const auto& subcfg: nlocfg) {
      get_singlebtag_config(subcfg.at(CFG_GROUP), cfg_path.stem());
    }
  } else {
    if (has_cfg_group(nlocfg)) nlocfg = nlocfg.at(CFG_GROUP);
    get_singlebtag_config(nlocfg, cfg_path.stem());
  }

  return 0;
}
